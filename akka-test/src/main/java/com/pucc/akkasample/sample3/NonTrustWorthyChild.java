package com.pucc.akkasample.sample3;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;

public class NonTrustWorthyChild extends AbstractLoggingActor {
    private long messages = 0L;

    /**
     * 给Actor绑定消息，类初始化的时候
     */ {
        receive(
                ReceiveBuilder
                        .match(Command.class,
                                this::onCommand)
                        .match(String.class,s -> context().parent().tell(s,sender()))
                        .build()
        );
    }

    /**
     * 给外界提供生成该Actor配置的接口
     * @return
     */
    public static Props props() {
        return Props.create(NonTrustWorthyChild.class);
    }

    /**
     * 消息处理的逻辑
     *
     * @param command
     */
    private void onCommand(Command command) {
        messages++;
        if (messages % 4 == 0) {
            throw new RuntimeException("Oh no, I got four commands,I can`t handle any more!");

        } else {
            log().info("Got a command!" + messages);
        }
    }

    /**
     * 提供给外界该Actor接受的消息类型
     */
    public static class Command {
    }
}
