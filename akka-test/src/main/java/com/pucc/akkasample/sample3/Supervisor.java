package com.pucc.akkasample.sample3;

import akka.actor.*;
import akka.japi.pf.DeciderBuilder;
import akka.japi.pf.ReceiveBuilder;
import scala.concurrent.duration.Duration;

/**
 * /user/supervisor/child
 */
public class Supervisor extends AbstractLoggingActor {

    /**
     * 自定义策略
     */
    public static final SupervisorStrategy STRATEGY = new OneForOneStrategy(
            10,
            Duration.create("10 seconds"),
            DeciderBuilder
                    .match(RuntimeException.class, ex -> SupervisorStrategy.stop())
                    .build()
    );

    /**
     * 初始化Actor
     */ {
        final ActorRef child = getContext().actorOf(NonTrustWorthyChild.props(), "child");
        /** 转发消息给子Actor */
        receive(
                ReceiveBuilder
                        .match(String.class,s->log().info(s))
                        .matchAny(any -> child.forward(any, getContext()))
                        .build()
        );
    }

    public static Props props() {
        return Props.create(Supervisor.class);
    }

    /**
     * 监视子Actor的策略
     * 1.只对出现问题的Actor进行处理
     * 2.对所有子Actor进行处理
     * An Akka SupervisorStrategy is the policy to apply for crashing children.
     * you should not normally need to create new subclasses, instead use the existing OneForOneStrategy or AllForOneStrategy,
     *
     * @return
     */
    @Override
    public SupervisorStrategy supervisorStrategy() {
        return STRATEGY;
    }

}
