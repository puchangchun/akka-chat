package com.pucc.akkasample.sample3;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import scala.io.StdIn$;

public class App {
    public static void main(String[] args) {
        /** actor 执行的系统 */
        ActorSystem system = ActorSystem.create("sample3");
        /** 添加actor */
        final ActorRef supervisor = system.actorOf(Supervisor.props(), "supervisor");
        ActorSelection actorSelection = system.actorSelection("/user/supervisor/child");

        for (int i = 0; i < 2; i++) {
            actorSelection.tell("new NonTrustWorthyChild.Command()",null);
            actorSelection.tell(new NonTrustWorthyChild.Command(),null);
        }


        System.out.println("ENTER to terminate");
        StdIn$.MODULE$.readLine();
        system.terminate();
        System.out.println("System is shutdown");
    }
}
