package com.pucc.akkasample.sample1;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import scala.io.StdIn$;

/**
 * akka
 * 1.actor
 * 2.message
 * 3.mail
 */
public class App {
    public static void main(String[] args) {
        /** actor 执行的系统 */
        ActorSystem system = ActorSystem.create("sample1");

        /** 提交actor到执行的系统，并获得他的引用 */
        final ActorRef counter = system.actorOf(Counter.props(), "counter");

        /** 告诉actor多条消息，并给出发送者; 一个线程发送5条消息 */
        for (int i = 0; i < 5; i++) {
            new Thread(
                    () -> {
                        // running block
                        for (int j = 0; j < 5; j++) {
                            counter.tell(new Counter.Message(), ActorRef.noSender());
                        }
                    }
            ).start();
        }


        System.out.println("ENTER to terminate");
        StdIn$.MODULE$.readLine();
        system.shutdown();
        System.out.println("System is shutdown");
    }


    /**
     * 这个例子是对比Thread模型，多个Thread去修改一个变量，
     * 通常我们需要设置同步块，因为java的内存模型会导致丢失修改、读脏数据
     * 但是Actor模型是完全异步的，Actor的内部状态只由它自己控制
     * 所以我们可以在多个线程里发送消息给对应的Actor,不需要管理它操作的同步
     */
    static class Counter extends AbstractLoggingActor {
        private int counter = 0;

        /**
         * initial in to new an class
         */ {
            receive(ReceiveBuilder
                    .match(Message.class, this::onMessage)
                    .build()
            );
        }

        /**
         * Props is a configuration object using in creating an Actor;
         * it is immutable,so it is thread-safe and fully shareable.
         * <p>
         * 生成包装Actor的Prop的一个工厂方法
         *
         * @return Props
         */
        public static Props props() {
            return Props.create(Counter.class);
        }

        private void onMessage(Message message) {
            this.counter++;
            log().info("Increased counter " + counter);
        }

        static class Message {
        }

    }
}
