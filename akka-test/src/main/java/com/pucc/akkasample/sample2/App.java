package com.pucc.akkasample.sample2;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import scala.PartialFunction;
import scala.io.StdIn$;
import scala.runtime.BoxedUnit;

public class App {
    public static void main(String[] args) {
        /** actor 执行的系统 */
        ActorSystem system = ActorSystem.create("sample2");
        /** 添加actor */
        final ActorRef alarm = system.actorOf(Alarm.props("123"), "alarm");

        alarm.tell(new Alarm.Activity(), null);
        alarm.tell(new Alarm.Disable("123"), null);
        alarm.tell(new Alarm.Enable("111"), null);
        alarm.tell(new Alarm.Enable("123"), null);
        alarm.tell(new Alarm.Activity(), null);
        alarm.tell(new Alarm.Disable("111"), null);
        alarm.tell(new Alarm.Disable("123"), null);


        System.out.println("ENTER to terminate");
        StdIn$.MODULE$.readLine();
        system.terminate();
        System.out.println("System is shutdown");
    }

    /**
     * Actor通过外界的消息来改变内部的状态
     * 这个是一个警报器的实现
     */
    static class Alarm extends AbstractLoggingActor {
        private final String password;
        PartialFunction<Object, BoxedUnit> enabled;
        PartialFunction<Object, BoxedUnit> disabled;


        public Alarm(String password) {
            this.password = password;

            //报警器开启时的状态
            enabled = ReceiveBuilder
                    .match(Disable.class,// 关闭警报器的消息
                            //lambda: onDisable()
                            disable -> {
                                if (disable.password.equals(this.password)) {
                                    getContext().become(disabled);
                                    log().info("the alarm is disabled !");
                                } else {
                                    log().warning("someone who don`t know this password try to disable the alarm!!");
                                }
                            })
                    .match(Activity.class,// 报警的消息
                            //lambda: onActivity
                            activity -> log().warning("eoeoeoeoeoe,alarm alarm!!"))
                    .build();

            //报警器关闭时的状态
            disabled = ReceiveBuilder
                    .match(Enable.class,// 打开警报器的消息
                            //lambda: onEnable
                            enable -> {
                                if (enable.password.equals(this.password)) {
                                    getContext().become(this.enabled);
                                    log().info("the alarm is enabled");
                                } else {
                                    log().warning("someone who don`t know this password try to enable the alarm!!");
                                }
                            })
                    .build();

            //initial receiver
            receive(disabled);
        }

        /**
         * 设计模式，由Actor创建自己的运行环境
         *
         * @param password
         * @return
         */
        public static Props props(String password) {
            return Props.create(Alarm.class, password);
        }

        //激活警报的消息
        static class Activity {
        }

        //开启警报的消息
        static class Enable {
            private final String password;

            public Enable(String password) {
                this.password = password;
            }
        }

        //关闭警报的消息
        static class Disable {
            private final String password;

            public Disable(String password) {
                this.password = password;
            }
        }
    }
}
