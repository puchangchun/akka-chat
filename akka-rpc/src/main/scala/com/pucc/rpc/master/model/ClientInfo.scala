package com.pucc.rpc.master.model

import akka.actor.ActorRef

case class ClientInfo(var name: String, var sex: String, var age: Int, var location: String, clientRef:ActorRef, var lastHeartbeatTime:Long)
