package com.pucc.rpc.master

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

import scala.io.StdIn

object MasterApp {
  def main(args: Array[String]): Unit = {
    /** Initial ActorSystem : root actor */
    val host = args(0)
    val port = args(1).toInt

    /** 指定跨进程的Actor通信，并给ActorSystem绑定本地IP和端口 */
    val configStr =
      s"""
         |akka.actor.provider="akka.remote.RemoteActorRefProvider"
         |akka.remote.netty.tcp.hostname="$host"
         |akka.remote.netty.tcp.port="$port"
         |""".stripMargin
    val config = ConfigFactory.parseString(configStr)

    val system = ActorSystem.create("MasterSystem", config)

    /** 创建master actor */
    val master = system.actorOf(Master.props(host,port,"master"), "master")


    println("ENTER to terminate")
    StdIn.readLine();
    system.terminate()
  }
}
