package com.pucc.rpc.master

import akka.actor.{Actor, Props}
import com.pucc.rpc.master.model.ClientInfo
import com.pucc.rpc.message.chat.{ClientsInfoMessage, SendClientsInfo, ToAllClientMessage}
import com.pucc.rpc.message.cluster.{CheckTimeOutClientMessage, HeartbeatMessage, RegisterMessage, RegisteredBackMessage}

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class Master(val host: String, val port: Int, val name: String) extends Actor {
  val masterUrl = s"akka.tcp://MasterSystem@$host:$port/user/$name"
  val clientInfoTable: mutable.HashMap[String, ClientInfo] = scala.collection.mutable.HashMap[String, ClientInfo]()
  val CHECK_INTERVAL = 2000 // 客户端掉线检测

  /**
   * Actor 启动之前调用
   */
  override def preStart(): Unit = {
    context.system
      .scheduler
      .schedule( // 开启检测客户端连接的定时任务
        0 millis,
        CHECK_INTERVAL millis,
        self,
        CheckTimeOutClientMessage
      )
  }

  /**
   * Actor 停止之后调用
   */
  override def postStop(): Unit = {
  }

  /**
   * 用于接收消息
   *
   * @return
   */
  override def receive: Receive = {
    /** 客户端注册消息处理 */
    case RegisterMessage(clientID, name, sex, age, location) =>
      println(s"received the client connection request")
      clientInfoTable.get(clientID) match {
        case Some(x) => // 已经建立连接
          sender() ! "This client has connected !"
        case None => // 开始建立连接
          clientInfoTable.put(clientID, ClientInfo(name, sex, age, location, sender(), System.currentTimeMillis()))
          println(s"$clientID:$name connected")
          sender() ! RegisteredBackMessage(masterUrl) // 返回客户端消息
      }
      self ! SendClientsInfo

    /** 心跳消息处理 */
    case HeartbeatMessage(clientID) =>
      clientInfoTable.get(clientID) match {
        case Some(x) =>
          println(s"${x.name}:发送心跳")
          x.lastHeartbeatTime = System.currentTimeMillis()
        case None =>
          println(s"$clientID 已经断开连接，却收到了心跳")
      }

    /** 检测客户端的连接 */
    case CheckTimeOutClientMessage =>
      val currentTime = System.currentTimeMillis()
      clientInfoTable
        .filter(currentTime - _._2.lastHeartbeatTime > CHECK_INTERVAL)
        .foreach(clientInfoTable -= _._1)
      println("检测客户端的连接：")
      clientInfoTable.foreach(println(_))

    /** 发送聊天室信息给客户端 */
    case SendClientsInfo =>
      val clientsInfoMessage = ClientsInfoMessage(clientInfoTable.toMap)
      clientInfoTable.foreach(_._2.clientRef ! clientsInfoMessage)

    /** 处理群发消息 */
    case msg:ToAllClientMessage=>
      clientInfoTable.foreach(_._2.clientRef ! msg)
  }

}

object Master {

  /**
   * get props
   *
   * @return
   */
  def props(host: String, port: Int, name: String): Props = {
    Props(new Master(host, port, name))
    // Props[Master]
  }
}
