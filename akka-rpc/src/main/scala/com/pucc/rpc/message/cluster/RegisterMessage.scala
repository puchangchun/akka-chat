package com.pucc.rpc.message.cluster

case class RegisterMessage(clientID: String, name: String, sex: String, age: Int, location: String)