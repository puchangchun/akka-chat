package com.pucc.rpc.message.chat

import com.pucc.rpc.master.model.ClientInfo


case class ClientsInfoMessage(clientInfoTable:Map[String,ClientInfo])
