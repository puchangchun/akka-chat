package com.pucc.rpc.message.cluster

/**
 *
 * @param masterUrl
 */
case class RegisteredBackMessage(masterUrl: String)
