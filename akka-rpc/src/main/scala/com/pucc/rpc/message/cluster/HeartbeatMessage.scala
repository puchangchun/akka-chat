package com.pucc.rpc.message.cluster

case class HeartbeatMessage(clientID: String)
