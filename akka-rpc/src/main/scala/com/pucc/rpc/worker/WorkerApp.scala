package com.pucc.rpc.worker

import akka.actor.ActorSystem
import com.pucc.rpc.message.chat.{SendAllClientMessage, ToAllClientMessage}
import com.typesafe.config.ConfigFactory

import scala.io.StdIn

/**
 * 在给定
 */
object WorkerApp {
  def main(args: Array[String]): Unit = {
    val name:String = args(0)
    val sex:String = args(1)
    val age:Int = args(2).toInt
    val location:String = args(3)
    /** Initial ActorSystem : root actor */
    val configStr =
      s"""
         |akka.actor.provider="akka.remote.RemoteActorRefProvider"
         |""".stripMargin
    val config = ConfigFactory.parseString(configStr)
    val system = ActorSystem.create("MasterSystem", config)

    /** 创建 worker actor */
    val worker = system.actorOf(Worker.props(name,sex, age, location), name.trim)


    /** 开启聊天室 */
    while (true){
      val context = StdIn.readLine();
      worker ! SendAllClientMessage(context)
    }

    system.terminate()
  }

}
