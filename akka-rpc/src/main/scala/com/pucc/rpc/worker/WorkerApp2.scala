package com.pucc.rpc.worker

import akka.actor.ActorSystem
import com.pucc.rpc.message.chat.SendAllClientMessage
import com.typesafe.config.ConfigFactory

import scala.io.StdIn


object WorkerApp2 {
  def main(args: Array[String]): Unit = {
//    val name:String = args(0)
//    val sex:String = args(1)
//    val age:Int = args(2).toInt
//    val location:String = args(3)
    /** Initial ActorSystem : root actor */
    val configStr =
      s"""
         |akka.actor.provider="akka.remote.RemoteActorRefProvider"
         |akka.remote.netty.tcp.hostname="172.27.171.206"
         |akka.remote.netty.tcp.port="2553"
         |""".stripMargin
    val config = ConfigFactory.parseString(configStr)
    val system = ActorSystem.create("MasterSystem", config)

    /** 创建 worker actor */
    val worker = system.actorOf(Worker.props("aa"),"aa" )


    /** 开启聊天室 */
    while (true){
      val context = StdIn.readLine();
      worker ! SendAllClientMessage(context)
    }


    println("ENTER to terminate")
    StdIn.readLine();
    system.terminate()
  }

}
