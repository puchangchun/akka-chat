package com.pucc.rpc.worker

import java.util.UUID

import akka.actor.{Actor, ActorSelection, Props}
import com.pucc.rpc.master.model.ClientInfo
import com.pucc.rpc.message.chat.{ClientsInfoMessage, SendAllClientMessage, ToAllClientMessage}
import com.pucc.rpc.message.cluster.{HeartSchedulerMessage, HeartbeatMessage, RegisterMessage, RegisteredBackMessage}

import scala.concurrent.duration._

/**
 * 一个客户端的代理
 */
class Worker(name: String, sex: String, age: Int, location: String) extends Actor {
  val clientID = UUID.randomUUID().toString
  val CHECK_INTERVAL = 1000
  /** 远程master的代理引用（_会给对应Type赋值默认值 Int:0 ；Ref:null） */
  var masterRef: ActorSelection = _
  var clientsInfo: Map[String, ClientInfo] = _

  import scala.concurrent.ExecutionContext.Implicits.global

  /**
   * 在启动之前和远端的Master建立连接
   */
  override def preStart(): Unit = {

    masterRef = context.actorSelection("akka.tcp://MasterSystem@172.27.171.206:8888/user/master")
    masterRef ! RegisterMessage(clientID, name, sex, age, location)
  }


  /**
   * 注册消息
   *
   * @return
   */
  override def receive: Receive = {
    /** 与master连接成功 */
    case RegisteredBackMessage(masterUrl) =>
      context.system
        .scheduler // akka的定时任务
        .schedule(
          0 millis,
          CHECK_INTERVAL millis,
          self,
          HeartSchedulerMessage) // 开启周期发送消息的任务

    /** 发送心跳消息 */
    case HeartSchedulerMessage =>
      masterRef ! HeartbeatMessage(clientID)

    /** 接受master传来的聊天室信息 */
    case ClientsInfoMessage(clientInfoTable) =>
      clientsInfo = clientInfoTable
      println("当前在线人员：")
      clientsInfo.foreach(x => println(x._2))

    /** 给聊天室发送消息 */
    case SendAllClientMessage(context) => {
      masterRef ! ToAllClientMessage(clientID = clientID, context = context)
    }

    /** 接受聊天室的信息 */
    case ToAllClientMessage(clientID, context) =>
      clientsInfo.get(clientID) match {
        case Some(x) => println(s"${x.name}(${x.location}):$context")
        case None =>
      }
  }

}

object Worker {

  def props(name: String = "游客", sex: String = "男", age: Int = 18, location: String = "四川南充"): Props = {
    Props(new Worker(name, sex, age, location))
  }
}
